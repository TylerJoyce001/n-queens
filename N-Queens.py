def filter_available_sqaures(candidate_sqaures, queen):
    available_squares = []
    for sqaure in candidate_sqaures:
        if sqaure[0] == queen[0]: continue
        if sqaure[1] == queen[1]:continue
        if sum(sqaure) == sum(queen): continue
        if sqaure[0] - sqaure[1] == queen[0] - queen[1]: continue
        available_squares.append(sqaure)
    return available_squares

N = 4
for column in range(N):
    available_squares = [(r,c) for r in range(N) for c in range(N)]
    queen = (0, column)
    queens = [queen]
    available_squares = filter_available_sqaures(available_squares, queen)
    while len(available_squares) > 0:
        queen = available_squares[0]
        queens.append(queen)
        available_squares = filter_available_sqaures(available_squares, queen)
    if len(queens) == N:
        break
    len(queens) == N
